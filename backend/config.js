'use strict';

var cfg = {};

cfg.dbConnectionString = 'mongodb://localhost/playground';

// for CORS
cfg.whitelist = [
	'http://localhost:9000',
	'http://0.0.0.0:9000',
];

module.exports = cfg;
