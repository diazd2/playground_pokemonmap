'use strict';

var express = require('express');
var $_ = require('../functions');
var lib = {
	'map': require('../bin/maps')
};
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	res.send('OK!');
});

/* GET: mapfile of all sprites */
router.get('/spritemap', $_.invoke(lib.map.getSprites, false));

module.exports = router;
