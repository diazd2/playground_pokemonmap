'use strict';

var cfg = require('./config');
var client = require('mongodb').MongoClient;

module.exports = function(next){
	client.connect(cfg.dbConnectionString, function(err, db) {
		if(!err) {
			next(db);
		} else {
			console.error(err);
		}
	});
};
