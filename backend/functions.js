'use strict';

var $_ = {};	// final name app-wide
var cfg = require('./config');

/* @code Integer. Defines HTTP error code.
 * @error String. Error message. No spaces. e.g: 'NotLoggedIn'
 * @message String. Descriptive message for error
 *
 * Defines a constant structure for errors to be sent to user
 * when an HTTP request is rejected
 */
$_.errorMsg = function(code, error, message){
	return {
		code: code || 500,
		error: error || 'UndefinedError',
		message: message || ''
	};
};

/* @f Function to be invoked. Receives request and response from Express.
 *   Also, @f receives [bodyParams], [queryParams], [request], and [response]
 *   as function parameters. Capture them only as needed
 * @loggedIn Defaults to TRUE. If true, requires user to be logged in
 *
 * Middleware-like function. Acts as a shorcut for calling
 * library and db functions for routes.
 */
$_.invoke = function(f, loggedIn){
	// default value for loggedIn (TRUE)
	loggedIn = (loggedIn === false)? loggedIn : true;
	// invoke function and return in middleware-like form
	return function(req, res) { //, next
		if(loggedIn && !req.user) {
			return res.status(401).send($_.errorMsg(401, 'NotLoggedIn'));
		}
		// check that all required fields have been set
		if(!f) {
			// 'f' function is not defined
			console.error('ERROR: function invoked (in $_.invoke) is undefined');
			return res.status(500).send();
		}
		// if 'requires' array is not defined, set to empty
		f.requires = f.requires || [];
		// check existance of parameters
		$_.checkPayloadParams(req.body, f.requires)
			.then(function(){
				try {
					// execute action
					f(req.body, req.query, req, res)
						.then(function(result){
							result = (result === null || typeof(result) === 'undefined')?
								{} : result;
							// return response as obtained from invoked function 'f'
							return res.json(result);
						})
						// rejected promises throw error responses
						.catch(function(err){
							return res.status(500).json(err);
						});
				} catch (e) {
					// f is not a function or error occurred
					console.error('ERROR: f invoked is not a function');
					return res.status(500).send();
				}
			})
			.catch(function(err){
				return res.status(err.code).json(err);
			});
	};
};

/* @f Function to be invoked. Receives request and response from Express.
 *   Also, @f receives [bodyParams], [queryParams], [request], and [response]
 *   as function parameters. Capture them only as needed
 *
 * Middleware-like function. Acts as a shorcut for calling
 * socket functions.
 */
$_.invokeOnSocket = function(io, socket, f){
	return function(data, callback){
		callback = (typeof(callback) === 'function') ? callback : function(){};
		if(cfg.levels[socket.session.level.name].disabled){
			callback(null, 'Realtime communication is disabled for this level');
			return;
		}
		if(!socket.session){
			callback(null, 'Not logged in');
			return;
		}
		// f is undefined, false, etc
		try {
			// data should contain the fields in this array
			f.requires = f.requires || [];
			for(var i = 0; i < f.requires.length; i++) {
				if(!data[f.requires[i]]) {
					callback(null, 'Missing param in data: ' + f.requires[i]);
				}
			}
			// params are ok, keep going...
			f(io, socket, data, callback);
		} catch (e) {
			callback(null, e);
		}
	};
};

/* @params Array/Object. Collection of params to check against @fields
 * @fields Array of Strings. These parameters must exist in @params
 *
 * Checks whether fields are set in params array/object. This
 * function should [ideally] be used only by $_.invoke()
 * Resolves if all fields are present.
 * Rejects with an error otherwise
 */
$_.checkPayloadParams = function(params, fields){
	return new Promise(function(resolve, reject){
		if(!fields || !fields.length) {
			return resolve();
		}
		if(!params) {
			return reject($_.errorMsg(400, 'NoParamsReceived'));
		}
		for(var i = 0; i < fields.length; i++) {
			if(!params[fields[i]]) {
				return reject($_.errorMsg(
					400,
					'MissingParam',
					'Parameter missing: ' + fields[i])
				);
			}
		}
		return resolve();
	});
};

module.exports = $_;
