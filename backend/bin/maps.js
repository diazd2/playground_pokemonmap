'use strict';

var lib = {};

lib.getSprites = function(){
	return new Promise(function(resolve, reject){
		try {
			var ret = JSON.parse(require('fs').readFileSync(__dirname + '/../spritemap.json', 'utf8'));
			if(ret.errno) {
				reject(ret.code);
			} else {
				resolve(ret);
			}
		} catch (e) {
			reject(e);
		}
	});
};

module.exports = lib;
