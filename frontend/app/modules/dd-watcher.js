'use strict';

(function(){
	angular.module('dd.watcher', [])

	.run([
		'Watcher',
		function(Watcher){
			Watcher.init();
		}
	])

	.provider('Watcher', function(){

		var config;

		this.set = function(initConfig){
			config = initConfig;
			config.uuid = config.uuid || 'ef9e4979-d5db-47e9-8ae4-0a15870ea8cd';
			config.watchers = config.watchers || [];
			config.waitTime = config.waitTime || 500;
		};

		this.$get = [
			'$rootScope',
			'$window',
			'$log',
			function ($rootScope, $window, $log) {
				return {
					init: function(){

						/* broadcasts 'windowResized' event */
						var watchWindowResize = function(){
							// Wait for persistent window.resize to end:
							var waitForResize = (function () {
								var timers = {};	// all timers (if multiple uuid's are used)
								return function (callback, ms) {
									if (timers[config.uuid]) {	// reset this timer
										clearTimeout (timers[config.uuid]);
									}
									timers[config.uuid] = setTimeout(callback, ms); // native js timeout
								};
							})();
							// on window.resize, broadcast event
							$(window).resize(function () {
								$rootScope.$apply(function(){
									$rootScope.resizing = true;
								});
								waitForResize(function(){
									$rootScope.$broadcast('windowResize', $window.innerWidth, $window.innerHeight);
									$rootScope.$apply(function(){
										$rootScope.resizing = false;
									});
								}, config.waitTime);
							});
						};

						// list of watchers
						var watchers = {
							'window-resize': watchWindowResize
						};

						config.watchers.forEach(function(watcherName){
							if(watchers[watcherName]) {
								watchers[watcherName]();
							} else {
								$log.warn('dd.Watcher: watcher ' + watcherName + ' does not exist');
							}
						});
					} // end init()
				}; // end return
			} // end function
		]; // end this.$get
	}); // end provider(Watcher)

})();

