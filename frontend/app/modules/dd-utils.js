'use strict';

(function(){
	angular.module('dd.utils', [])

	.directive('ngRepeatEmit', [
		'$timeout',
		function ($timeout) {
			return {
				restrict: 'A',
				link: function (scope) {
					if (scope.$last === true) {
						$timeout(function () {
							scope.$emit('ngRepeatFinished');
						});
					}
				}
			};
		}
	]) // end directive(ngRepeatFinished)


	.directive('ngEnter', function() {
		return function(scope, element, attrs) {
			element.bind('keydown keypress', function(event) {
				if(event.which === 13) {
					scope.$apply(function(){
						scope.$eval(attrs.ngEnter);
					});
					event.preventDefault();
				}
			});
		};
	}) // end directive(ngEnter)

	.service('KeyBind', [
		'$window',
		'$rootScope',
		function ($window, $rootScope) {
			var service = {};

			var bind = function(mode, keyCode, element, callback, scope){
				if(!element) {
					element = $(angular.element($window));
				}
				if(!scope) {
					scope = $rootScope;
				}
				if(!element.bind) {
					console.warn('Element has no bind() function');
					return;
				}
				element.bind(mode, function(event) {
					if(event.which === keyCode) {
						scope.$apply(function(){
							scope.$eval(callback);
						});
						event.preventDefault();
					}
				});
			};

			service.press = function(keyCode, element, callback, scope){
				bind('keypress', keyCode, element, callback, scope);
			};

			service.down = function(keyCode, element, callback, scope){
				bind('keydown', keyCode, element, callback, scope);
			};

			service.downpress = function(keyCode, element, callback, scope){
				bind('keydown keypress', keyCode, element, callback, scope);
			};

			return service;
		}
	]); // end service(KeyBind)

})();

