'use strict';

/**
 * @ngdoc overview
 * @name playGroundApp
 * @description
 * # playGroundApp
 *
 * Main module of the application.
 */
angular

.module('playGroundApp', [
	'dd.utils',
	'dd.watcher',
	'ngResource',
	'ngAnimate',
	'ngSanitize',
	'ui.router',
	'ui.bootstrap',
	'ui.event',
	'ui.mask',
	'ui.select',
	'ui.slider',
	'ui.sortable'
])

.config([
	'$stateProvider',
	'$urlRouterProvider',
	'$httpProvider',
	'$sceDelegateProvider',
	'WatcherProvider',
	'APIProvider',
	function ($stateProvider, $urlRouterProvider, $httpProvider, $sceDelegateProvider, WatcherProvider, APIProvider) {
		// set permanent watchers
		WatcherProvider.set({watchers: ['window-resize']});
		// init API provider
		APIProvider.setURL('http://localhost:8000/');
		// Mark same origin's reources as $sce safe
		$sceDelegateProvider.resourceUrlWhitelist([ 'self' ]);
		// Allow set-cookies
		$httpProvider.defaults.withCredentials = true;
		/* Not found or default */
		$urlRouterProvider.otherwise('/');
		/* ui.router states */
		$stateProvider
			.state('home', {
				url: '/',
				templateUrl: 'views/home.html',
				controller: 'HomeCtrl'
			})
			.state('builder', {
				url: '/builder',
				templateUrl: 'views/builder.html',
				params: {
					map: null
				},
				controller: 'BuilderCtrl'
			});
	}
])

.run([
	'$rootScope',
	'$http',
	function ($scope, $http) {
		// App version info
		$http.get('version.json').success(function (v) {
			$scope.version = v.version;
			$scope.appName = v.name;
		});
	}
]);
