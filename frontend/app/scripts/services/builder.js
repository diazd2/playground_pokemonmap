'use strict';

angular.module('playGroundApp')

/**
 * @ngdoc service
 * @name playGroundApp.Builder
 * @description
 * # Builder
 */

.service('Builder', [
	'$q',
	'$log',
	'Sprites',
	function($q, $log, Sprites){
		const MENU_BLOCKS_SIZE_MULTIPLIER = 1.5; // times larger than the default length of the sprite
		const GRID_INITIAL_X = 20;
		const GRID_INITIAL_Y = 20;
		const DRAW_TYPE_SINGLE = 'single';
		const DRAW_TYPE_AUTOFILL = 'fill';
		const DRAW_TYPE_AREA = 'area';
		const DRAW_TYPE_LINE = 'line';

		var currentAutoFilledSprite; // used for history
		var currentAutoFilledState; // used for history
		var drawType = DRAW_TYPE_SINGLE; // marks drawing mode
		var activeBlock; // this block is drawn in the grid
		var firstBlock; // used for area and line draw
		var error;
		var history = [];
		var map = [];


		var resetError = function(){
			error = null;
		};

		/* sets drawing mode. Also resets error and releases temp elements of every mode */
		var beginMode = function(mode){
			resetError();
			firstBlock = null;
			drawType = mode;
		};

		var canUndo = function(){
			return history.length > 0;
		};

		/* Adds entry to history log (used to undo changes)
		 * Valid types:
		 *   - 'drawBlock' (x, y, sprite, autoFilled) Single block added to builder-map
		 *   - 'fillAll' (sprite, autoFilled) All non-'filled' blocks are filled with single block
		 */
		var writeHistory = function(type, properties){
			history.push({ type: type, properties: properties });
		};

		/* Draws sprite on all untouched or auto-filled blocks in map */
		var fillAll = function(sprite, autoFilled, skipHistory){
			if(!skipHistory) {
				// add action to history (to enable 'undo')
				writeHistory(
					DRAW_TYPE_AUTOFILL,
					{ sprite:currentAutoFilledSprite, autoFilled: currentAutoFilledState }
				);
			}
			for(var i = 0; i < map.length; i++){
				for(var j = 0; j < map[i].length; j++){
					if(!map[i][j].sprite || map[i][j].autoFilled) {
						map[i][j].sprite = sprite;
						// mark this cell as filled so next time this function is
						// called, this cell is overwritten again
						map[i][j].autoFilled = autoFilled;
					}
				}
			}
			currentAutoFilledSprite = sprite;
			currentAutoFilledState = autoFilled;
		};
		/* Undoes last map auto-fill */
		var undoFillAll = function(record){
			currentAutoFilledSprite = record.properties.sprite;
			currentAutoFilledState = record.properties.autoFilled;
			fillAll(record.properties.sprite, record.properties.autoFilled, true);
		};

		// draws single block
		var drawBlock = function(gridBlock, undo, forceSkipUndoList){
			if(!undo && !forceSkipUndoList) {
				// add action to history (to enable 'undo')
				writeHistory(
					DRAW_TYPE_SINGLE,
					{ x:gridBlock.x, y:gridBlock.y }
				);
			}
			// unmark this block to stop marquee highlight
			delete gridBlock.active;
			if(undo) {
				if(gridBlock.history.length < 1) {
					delete gridBlock.sprite;
					delete gridBlock.autoFilled;
				} else {
					var prevBlockState = gridBlock.history.pop();
					gridBlock.autoFilled = prevBlockState.autoFilled;
					gridBlock.sprite = angular.copy(prevBlockState.sprite);
				}
			} else {
				gridBlock.history.push({
					autoFilled: gridBlock.autoFilled,
					sprite: angular.copy(gridBlock.sprite)
				});
				gridBlock.sprite = angular.copy(activeBlock);
				gridBlock.autoFilled = false;
				// the 'selected' property comes from the sprite block
				delete gridBlock.sprite.coords.selected;
			}
			// if manually edited, this cell is marked to be excluded from auto-fill
		};
		/* Undoes last block drawn in map */
		var undoDrawBlock = function(record){
			drawBlock(map[record.properties.x][record.properties.y], true);
		};

		/* fill rectangle contained between first and second block */
		var fillRectangle = function(first, second, undo){
			if(!undo){
				// write history
				writeHistory(
					DRAW_TYPE_AREA,
					{ first:first, second:second }
				);
			}
			for(var i = first[0]; i <= second[0]; i++) {
				for(var j = first[1]; j <= second[1]; j++) {
					drawBlock(map[i][j], undo, true);
				}
			}
		};

		// draws sprite on every block in the area from [x1,y1] to [x2,y2]
		var drawArea = function(block){
			// set firstBlock if not initialized
			if(!firstBlock) {
				firstBlock = block;
				block.active = true;
			} else {
				firstBlock.active = false;
				// this is the second block, we should now draw:
				var f, s;
				if(firstBlock.x >= block.x && firstBlock.y >= block.y){
					f = [block.x, block.y];
					s = [firstBlock.x, firstBlock.y];
				} else if (firstBlock.x < block.x && firstBlock.y > block.y) {
					f = [firstBlock.x, block.y];
					s = [block.x, firstBlock.y];
				} else if (firstBlock.x > block.x && firstBlock.y < block.y) {
					f = [block.x, firstBlock.y];
					s = [firstBlock.x, block.y];
				} else {
					f = [firstBlock.x, firstBlock.y];
					s = [block.x, block.y];
				}
				// fill rectangle
				fillRectangle(f, s);
				// release first block
				firstBlock = null;
				// reset mode
				// beginMode(DRAW_TYPE_SINGLE);
			}
		};

		var undoDrawArea = function(record){
			fillRectangle(
				record.properties.first,
				record.properties.second,
				true
			);
		};

		return {
			getError: function(){
				return error;
			},

			/* Returns drawing map for grid */
			getMap: function(){
				return map;
			},

			/* Used when editing a new map (generates an array to represent grid and store
			 * cells info (sprites drawn, etc)
			 */
			initMap: function(m){
				// create map
				return $q(function(resolve){
					if(m) {
						map = m;
						return resolve();
					}
					map = [];
					for(var i = 0; i < GRID_INITIAL_X; i++) {
						map.push([]);
						for (var j = 0; j < GRID_INITIAL_Y; j++) {
							map[i].push({
								x: i,
								y: j,
								history: []
							});
						}
					}
					resolve();
				});
			},

			/* The active block is used to draw in the grid. If this object is not set, the
			 * service.draw function exits with an error
			 */
			setActiveBlock: function(baseName, blockName){
				activeBlock = {
					base: baseName,
					block: blockName,
					coords: Sprites.map.land[baseName][blockName]
				};
			},
			getActiveBlock: function(){
				return activeBlock;
			},

			/* Draws single sprite (activeSprite) on map */
			draw: function(block){
				resetError();
				// draw!
				if(activeBlock) {
					switch(drawType){
						// single block
						case DRAW_TYPE_SINGLE:
							drawBlock(block);
							break;
						// area
						case DRAW_TYPE_AREA:
							drawArea(block);
							break;
						// line
						case DRAW_TYPE_LINE:
							// drawLine(block);
							break;
					}
				} else {
					error = 'No block selected';
				}
			},

			/* Fills every cell in the grid with active block. Filled cells' 'autoFilled' property
			 * is set to TRUE. Cells marked with property 'autoFilled' = FALSE are ignored
			 */
			autoFill: function(){
				resetError();
				if(activeBlock) {
					fillAll(activeBlock, true);
				} else {
					error = 'No block selected';
				}
			},

			/* Returns the background-position of the received block, based on its position
			 * in the spritesheet
			 */
			getBlockStyle: function(block){
				// blocks in menu, used to draw (their coords are included in the block itself)
				if(block && !block.sprite) {
					return {
						'background-position-y': block[0] * Sprites.map.length * MENU_BLOCKS_SIZE_MULTIPLIER * -1,
						'background-position-x': block[1] * Sprites.map.length * MENU_BLOCKS_SIZE_MULTIPLIER * -1
					};
				// for drawn blocks. The sprite is a referenced property of the drawable blocks
				} else if(block && block.sprite) {
					return {
						'background-position-y': block.sprite.coords[0] * Sprites.map.length * -1,
						'background-position-x': block.sprite.coords[1] * Sprites.map.length * -1
					};
				}
			},

			/* Returns current active draw-type (see const's, with DRAW_TYPE_ prefix */
			getDrawType: function(){
				return drawType;
			},

			/* Begins "single"-block drawing. Requires 1 click
			 */
			beginSingleDraw: function(){
				beginMode(DRAW_TYPE_SINGLE);
			},

			/* Begins "area" drawing. Requires 2 clicks:
			 * Area contained from [x1,y1] to [x2,y2] is filled with active block
			 */
			beginAreaDraw: function(){
				beginMode(DRAW_TYPE_AREA);
			},

			/* Begins "line" drawing. Requires 2 clicks:
			 * Straight line (horizontal, vertical, diagonal) contained from [x1,y1] to [x2,y2]
			 * is filled with active block
			 */
			beginLineDraw: function(){
				drawType = DRAW_TYPE_LINE;
			},

			/* TRUE if the history array is not empty */
			canUndo: function(){
				return canUndo();
			},

			/* Undoes the last action (passed as a service function, but internally calls several
			 * others depending on the type of action to undo)
			 */
			undo: function(){
				resetError();
				if(canUndo()) {
					var currentRecord = history.pop();
					switch(currentRecord.type) {
						// single block
						case DRAW_TYPE_SINGLE:
							undoDrawBlock(currentRecord);
							break;
						// auto-fill
						case DRAW_TYPE_AUTOFILL:
							undoFillAll(currentRecord);
							break;
						// area-drawing
						case DRAW_TYPE_AREA:
							undoDrawArea(currentRecord);
							break;
					}
				} else {
					error = 'Nothing to undo';
				}
			},

			/* NOT READY */
			save: function(){
				resetError();
				error = 'Save function not ready';
			}


		};

	}
]);
