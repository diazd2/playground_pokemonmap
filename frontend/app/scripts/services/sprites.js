'use strict';

angular.module('playGroundApp')

/**
 * @ngdoc service
 * @name playGroundApp.Sprites
 * @description
 * # Sprites
 */

.service('Sprites', [
	'$log',
	'API',
	function($log, API){
		var service = {};

		service.init = function(){
			service.url = API.getURL() + 'spritemap.jpg';
			return API
			.getSpriteMap()
			.then(function(result){
				service.map = result.data;
			})
			.catch(function(){
				$log.warn('Could not load map of sprites');
			});
		};

		return service;
	}
]);
