'use strict';

/**
 * @ngdoc function
 * @name playGroundApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 */
angular.module('playGroundApp')
.controller('HomeCtrl', [
	'$scope',
	'$state',
	function ($scope, $state) {
		$scope.create = function(){
			$state.go('builder');
		};
	}
]);
