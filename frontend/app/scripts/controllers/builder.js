'use strict';

/**
 * @ngdoc function
 * @name playGroundApp.controller:BuilderCtrl
 * @description
 * # BuilderCtrl
 */
angular.module('playGroundApp')
.controller('BuilderCtrl', [
	'$scope',
	'$stateParams',
	'Builder',
	'Sprites',
	'KeyBind',
	function ($scope, $stateParams, Builder, Sprites, KeyBind) {
		const UP = 0;
		const DOWN = 1;
		const LEFT = 2;
		const RIGHT = 3;
		const BLOCKS_PER_ROW = 5;

		// returns an array containing [0] the name of the last element of obj,
		// and [1] the value of such element
		var lastInObject = function(obj){
			var length = Object.keys(obj).length;
			return [
				Object.keys(obj)[length-1],
				obj[Object.keys(obj)[length-1]]
			];
		};

		// selects the next block above the active block as the new active
		var selectBlockUp = function(common){
			var nextGroupName, nextGroup, nextBlockName, nextBlock;
			// find out if this is the first row in the group
			if(common.rowNumber === 0) {
				nextGroupName = Object.keys($scope.baseBlocks)[common.groupNumber-1];
				nextGroup = $scope.baseBlocks[nextGroupName];
				// check if next group has a total number of blocks who is as least the
				// same as any multiple of the current active block's position
				if((Object.keys(nextGroup).length % BLOCKS_PER_ROW) > common.positionInRow) {
					nextBlockName = Object.keys(nextGroup)[common.positionInRow];
					nextBlock = nextGroup[nextBlockName];
				// if not, select the last element of the next group instead
				} else {
					var nextGroupLastObject = lastInObject(nextGroup);
					nextBlockName = nextGroupLastObject[0];
					nextBlock = nextGroupLastObject[1];
				}
				return [
					nextGroupName,
					nextBlock,
					nextBlockName
				];
			} else {
				var nextPositionInRow = (common.rowNumber - 1) * BLOCKS_PER_ROW + common.positionInRow;
				nextBlockName = Object.keys(common.group)[nextPositionInRow];
				nextBlock = common.group[nextBlockName];
				return [
					common.activeBlock.base,
					nextBlock,
					nextBlockName
				];
			}
		};

		// selects the next block below the active block as the new active
		var selectBlockDown = function(common){
			var nextGroupName, nextGroup, nextBlockName, nextBlock;
			// find out if this is the last row in the group
			if(common.rowNumber === (common.rowsInGroup - 1)) {
				nextGroupName = Object.keys($scope.baseBlocks)[common.groupNumber+1];
				nextGroup = $scope.baseBlocks[nextGroupName];
				// check if next group has at least the same number of blocks
				if(Object.keys(nextGroup).length >= (common.positionInRow + 1)) {
					nextBlockName = Object.keys(nextGroup)[common.positionInRow];
					nextBlock = nextGroup[nextBlockName];
				// if not, select the last element of the next group instead
				} else {
					var nextGroupLastObject = lastInObject(nextGroup);
					nextBlockName = nextGroupLastObject[0];
					nextBlock = nextGroupLastObject[1];
				}
				return [
					nextGroupName,
					nextBlock,
					nextBlockName
				];
			} else {
				var nextPositionInRow = common.positionInRow + BLOCKS_PER_ROW;
				// check if this row has enough elements to move down vertically
				if(Object.keys(common.group).length > nextPositionInRow) {
					nextBlockName = Object.keys(common.group)[nextPositionInRow];
					nextBlock = common.group[nextBlockName];
				} else {
					var lastInRow = lastInObject(common.group);
					nextBlockName = lastInRow[0];
					nextBlock = lastInRow[1];
				}
				return [
					common.activeBlock.base,
					nextBlock,
					nextBlockName
				];
			}
		};

		// selects the next block to the left of the active block as the new active
		var selectBlockLeft = function(common){
			// find out if this is the first block in the row
			if(common.positionInGroup > 0) {
				return [
					common.activeBlock.base,
					common.group[Object.keys(common.group)[common.positionInGroup-1]],
					Object.keys(common.group)[common.positionInGroup-1]
				];
			} else {
				var nextGroupName = Object.keys($scope.baseBlocks)[common.groupNumber-1];
				var nextGroupLastObject = lastInObject($scope.baseBlocks[nextGroupName]);
				var nextBlockName = nextGroupLastObject[0];
				var nextBlock = nextGroupLastObject[1];
				return [
					nextGroupName,
					nextBlock,
					nextBlockName
				];
			}
		};

		// selects the next block to the right of the active block as the new active
		var selectBlockRight = function(common){
			// find out if this is the last block in the row
			if(common.positionInGroup < (Object.keys(common.group).length - 1)) {
				return [
					common.activeBlock.base,
					common.group[Object.keys(common.group)[common.positionInGroup+1]],
					Object.keys(common.group)[common.positionInGroup+1]
				];
			} else {
				var nextGroupName = Object.keys($scope.baseBlocks)[common.groupNumber+1];
				var nextBlockName = Object.keys($scope.baseBlocks[nextGroupName])[0];
				var nextBlock = $scope.baseBlocks[nextGroupName][nextBlockName];
				return [
					nextGroupName,
					nextBlock,
					nextBlockName
				];
			}
		};

		// Selects a block relative to the position of the current active block. @direction
		// can be UP, DOWN, LEFT, RIGHT
		var selectBlockRelative = function(direction){
			var params;
			var activeBlock = Builder.getActiveBlock();
			// position of group (type) in sprite map
			var group = $scope.baseBlocks[activeBlock.base];
			var groupNumber = Object.keys($scope.baseBlocks).indexOf(activeBlock.base);
			var positionInGroup = Object.keys(group).indexOf(activeBlock.block);
			var rowsInGroup = Math.ceil(Object.keys(group).length / BLOCKS_PER_ROW);
			// position of row in group
			var rowNumber = Math.floor(positionInGroup / BLOCKS_PER_ROW);
			// position of active block in group
			var positionInRow = positionInGroup % BLOCKS_PER_ROW;
			var common = {
				activeBlock: activeBlock,
				group: group,
				groupNumber: groupNumber,
				positionInGroup: positionInGroup,
				rowsInGroup: rowsInGroup,
				rowNumber: rowNumber,
				positionInRow: positionInRow
			};
			try {
				switch(direction) {
					case UP:
						params = selectBlockUp(common);
						break;
					case DOWN:
						params = selectBlockDown(common);
						break;
					case LEFT:
						params = selectBlockLeft(common);
						break;
					case RIGHT:
						params = selectBlockRight(common);
						break;
				}
				$scope.select(
					params[0],
					params[1],
					params[2]
				);
			} catch(e) {
				// console.warn('Cannot move to next block in that direction');
			}
		};

		/* KEY BINDINGS */
		(function(){
			// 26 = ctrl+z
			KeyBind.press(26, null, function(){
				Builder.undo();
			});
			// 119 = w
			KeyBind.downpress(119, null, function(){
				selectBlockRelative(UP);
			});
			// 115 = s
			KeyBind.downpress(115, null, function(){
				selectBlockRelative(DOWN);
			});
			// 97 = a
			KeyBind.downpress(97, null, function(){
				selectBlockRelative(LEFT);
			});
			// 100 = d
			KeyBind.downpress(100, null, function(){
				selectBlockRelative(RIGHT);
			});
			// 113 = q
			KeyBind.downpress(113, null, function(){
				Builder.beginAreaDraw();
			});
			// 101 = e
			KeyBind.downpress(101, null, function(){
				Builder.beginSingleDraw();
			});
			// 102 = f
			KeyBind.downpress(102, null, function(){
				Builder.autoFill();
			});
		})();

		var storage = {
			selectedBlock: null // keep a link to the selected block to handle its state
		};
		$scope.loading = true;
		$scope.getBlockStyle = Builder.getBlockStyle;
		$scope.getError = Builder.getError;
		$scope.save = Builder.save;

		// initialize Sprite objects
		Sprites.init().then(function(){
			// set base building blocks
			$scope.baseBlocks = Sprites.map.land;
			// selects first block as initial active block
			var firstGroup = $scope.baseBlocks[Object.keys($scope.baseBlocks)[0]];
			$scope.select(
				Object.keys($scope.baseBlocks)[0], // first group's name
				firstGroup[Object.keys(firstGroup)[0]], // first block
				Object.keys(firstGroup)[0] // first block's name
			);
			// if map was passed to the state controller, load it
			if($stateParams.map) {
				Builder.setMap($stateParams.map);
				$scope.map = Builder.getMap();
			// if no map was passed, retrieve a new one
			} else {
				Builder.initMap().then(function(){
					$scope.map = Builder.getMap();
				});
			}
		});

		$scope.select = function(type, block, blockName) {
			if(storage.selectedBlock) {
				delete storage.selectedBlock.selected;
			}
			storage.selectedBlock = block;
			block.selected = true;
			Builder.setActiveBlock(type, blockName);
		};

		$scope.$on('ngRepeatFinished', function(){
			$scope.loading = false;
		});

	}
]);
