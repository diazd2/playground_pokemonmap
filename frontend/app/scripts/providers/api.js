'use strict';

angular.module('playGroundApp')

/**
 * @ngdoc provider
 * @name playGroundApp.APIProvider
 * @description Base provider. Global API services are managed here
 * # APIProvider
 */

.provider('API', function(){
	var URL = 'http://localhost/';

	this.setURL = function(url){
		URL = url || URL;
	};

	this.$get = [
		'$http',
		function($http) {
			var APIProvider = {};
			APIProvider.spriteMapURL = URL + 'spritemap';

			APIProvider.getURL = function(){
				return URL;
			};

			APIProvider.getSpriteMap = function(){
				return $http.get(APIProvider.spriteMapURL);
			};

			return APIProvider;
		}
	];
});