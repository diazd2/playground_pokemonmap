'use strict';

/**
 * @ngdoc directive
 * @name playGroundApp.directive:builderMap
 * @description
 * # builderMap
 */
angular.module('playGroundApp')
	.directive('builderMap', [
		'Builder',
		function(Builder) {
			return {
				restrict: 'E',
				templateUrl: 'views/directives/builder-map.html',
				scope: {
					map: '='	// contains initial map passed to editor. If null, a new map is initialized
				},
				link: function(scope) {
					scope.draw = Builder.draw;
					scope.undo = Builder.undo;
					scope.getDrawType = Builder.getDrawType;
					scope.getBlockStyle = Builder.getBlockStyle;
					scope.beginSingleDraw = Builder.beginSingleDraw;
					scope.beginAreaDraw = Builder.beginAreaDraw;
					scope.undo = Builder.undo;
					scope.fillAll = Builder.autoFill;

					scope.hideGridBorder = false;

					scope.toggleGrid = function(){
						scope.hideGridBorder = !scope.hideGridBorder;
					};

				} // end link function

			}; // end returned function

		} // end directive function
	]
);